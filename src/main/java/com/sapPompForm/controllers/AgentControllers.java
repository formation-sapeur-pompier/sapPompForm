package com.sapPompForm.controllers;

import com.sapPompForm.dto.AgentDto;
import com.sapPompForm.entities.Agent;
import com.sapPompForm.services.AgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/")
public class AgentControllers {
    @Autowired
    private AgentService agentService;


    @GetMapping("agents")
    public List<Agent> listAgents(){
        return agentService.allAgent();
    }

    @PostMapping("addAgent")
    public void registerNewAgent(@RequestBody AgentDto agent){
        agentService.addNewAgent(agent);
    }

    @DeleteMapping("deleteAgent/{idAgent}")
    public void deleteAgent(@PathVariable int idAgent){
        agentService.deleteAgent(idAgent);
    }

    @PutMapping("updateAgent")
    public void updateAgent(@RequestBody AgentDto agentDto) {
        agentService.updateAgent(agentDto);
    }

    @GetMapping("agent")
    public AgentDto getOneAgent(@RequestParam int idAgent) {
        return agentService.getOneAgent(idAgent);
    }
}

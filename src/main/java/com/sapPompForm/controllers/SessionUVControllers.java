package com.sapPompForm.controllers;

import com.sapPompForm.entities.SessionUV;
import com.sapPompForm.services.SessionUVService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api")
public class SessionUVControllers {
    @Autowired
    private SessionUVService sessionUVService;

    @GetMapping("/sessionUV")
    public List<SessionUV> listSession(){
        return sessionUVService.listSession();
    }

    @PostMapping("/sessionUV")
    public void newSessionUV(@RequestBody SessionUV sessionUV){
        sessionUVService.addNewSessionUV(sessionUV);
    }

    @PutMapping("/updateSessionUV")
    public void updateSession(@RequestBody SessionUV sessionUV){
        sessionUVService.updateSessionUV(sessionUV);
    }

    @DeleteMapping("/deleteSessionUV/{idSessionUV}")
    public void deleteSessionUV(@PathVariable int idSessionUV) {
        sessionUVService.deleteSessionUV(idSessionUV);
    }

    @GetMapping("/oneSessionUV/{idSessionUV}")
    public SessionUV oneSession(@PathVariable int idSessionUV) {
        return sessionUVService.oneSessionUV(idSessionUV);
    }

    @GetMapping("/currentSessionUV")
    public List<SessionUV> listCurrentSessionUV(){
        return sessionUVService.listCurrentSessionUV();
    }

    @GetMapping("/nextSessionUV")
    public List<SessionUV> listNextSessionUV(){
        return sessionUVService.listNextSessionUV();
    }
}

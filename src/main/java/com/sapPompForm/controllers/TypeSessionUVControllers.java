package com.sapPompForm.controllers;

import com.sapPompForm.entities.TypeSessionUV;
import com.sapPompForm.services.TypeSessionUVService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api")
public class TypeSessionUVControllers {
    @Autowired
    private TypeSessionUVService typeSessionUVService;

    @GetMapping("/typeSessionUV")
    public List<TypeSessionUV> listTypeSession(){
        return typeSessionUVService.listTypeSession();
    }

    @PostMapping("/typeSessionUV")
    public void newTypeSessionUV(@RequestBody TypeSessionUV typeSessionUV){
        typeSessionUVService.addNewTypeSessionUV(typeSessionUV);
    }

    @PutMapping("/updateTypeSessionUV")
    public void updateTypeSession(@RequestBody TypeSessionUV typeSessionUV) {
        typeSessionUVService.updateTypeSession(typeSessionUV);
    }

    @DeleteMapping("deleteTypeSessionUV/{idTypeSessionUV}")
    public void deleteTypeSession(@PathVariable int idTypeSessionUV){
        typeSessionUVService.deleteTypeSessionUV(idTypeSessionUV);
    }

}

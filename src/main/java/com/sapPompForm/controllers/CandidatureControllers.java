package com.sapPompForm.controllers;

import com.sapPompForm.entities.Candidature;
import com.sapPompForm.services.CandidatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api")
public class CandidatureControllers {
    @Autowired
    private CandidatureService candidatureService;


    @GetMapping("/candidatures")
    public List<Candidature> listCandidature(){
        return candidatureService.listCandidature();
    }

    @PostMapping("/candidature")
    public void newCandidature(@RequestBody Candidature candidature, @RequestParam int idAgent) {
        candidatureService.addNewCandidature(idAgent,candidature);
    }
/*
    @PutMapping("/updateCand")
    public void updateCandidature(@RequestBody Candidature candidature, @RequestParam int idAgent) {
        candidatureService.updateCandidature(idAgent, candidature);
    }
*/
    @PutMapping("/updateStatus/{idCandidature}/{status}")
    public void updateStatus(@PathVariable int idCandidature,@PathVariable String status) {
        candidatureService.updateStatus(idCandidature, status);
    }

    @DeleteMapping("/deleteCand/{idCandidature}")
    public void deleteCandidature(@RequestParam int idAgent, @PathVariable int idCandidature){
        candidatureService.deleteCandidature(idAgent, idCandidature);
    }

    @GetMapping("/oneCandidature/{idCandidature}")
    public Candidature oneCandidature (@PathVariable int idCandidature) {
        return candidatureService.oneCandidature(idCandidature);
    }
}

package com.sapPompForm.controllers;

import com.sapPompForm.entities.Prerequis;
import com.sapPompForm.services.PrerequisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api")
public class PrerequisControllers {
    @Autowired
    private PrerequisService prerequisService;

    @GetMapping("/prerequis")
    public List<Prerequis> listPrerequis(){
        return prerequisService.listPrerequis();
    }

    @PostMapping("/prerequis")
    public void newPrerequis(@RequestBody Prerequis prerequis){
        prerequisService.addNewPrerequis(prerequis);
    }

    @PutMapping("/updatePrerequis")
    public void updatePrerequis(@RequestBody Prerequis prerequis){
        prerequisService.updatePrerequis(prerequis);
    }

    @DeleteMapping("/deletePrerequis/{idPrerequis}")
    public void deletePrerequis(@PathVariable int idPrerequis) {
        prerequisService.deletePrerequis(idPrerequis);
    }

    @GetMapping("/onePrerequis/{idPrerequis}")
    public Prerequis onePrerequis(@PathVariable int idPrerequis) {
        return prerequisService.onePrerequis(idPrerequis);
    }
}

package com.sapPompForm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SapPompFormApplication {

	public static void main(String[] args) {
		SpringApplication.run(SapPompFormApplication.class, args);
	}


}

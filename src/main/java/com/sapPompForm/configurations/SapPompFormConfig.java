package com.sapPompForm.configurations;

import com.sapPompForm.Repositories.*;
import com.sapPompForm.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collection;
import java.util.List;

@Configuration
public class SapPompFormConfig {
    @Autowired
    private AgentRepository agentRepository;
    @Autowired
    private CandidatureRepository candidatureRepository;
    @Autowired
    private SessionUVRepository sessionUVRepository;
    @Autowired
    private TypeSessionUVRepository typeSessionUVRepository;
    @Autowired
    private PrerequisRepository prerequisRepository;

    @Bean
    CommandLineRunner commandLineRunner(){
        return args -> {
            Prerequis p1 = new Prerequis("T1");
            Prerequis p2 = new Prerequis("T2");

            //Agent a1 = new Agent("toto","toto",List.of(Role.ADMIN, Role.USER),List.of(),List.of(p2));
            Agent a1 = new Agent("ADMIN","toto",List.of(Role.ADMIN, Role.USER),List.of(),List.of());
            Agent a2 = new Agent("tata","tata",List.of(Role.USER),List.of(),List.of());
            Agent a3 = new Agent("titi","titi",List.of(Role.USER),List.of(),List.of());



            TypeSessionUV ts1 = new TypeSessionUV("T1",2,4,List.of());
            TypeSessionUV ts2 = new TypeSessionUV("T2",4,8,List.of(p1));
            TypeSessionUV ts3 = new TypeSessionUV("T3",4,8,List.of(p2));

            SessionUV s1 = new SessionUV("15/02/2021","18/02/2021",null ,ts1);
            SessionUV s2 = new SessionUV("20/02/2021","25/02/2021",null,ts2);
            SessionUV s3 = new SessionUV("19/02/2021","25/02/2021",null,ts3);

            Candidature c1 = new Candidature("EN ATTENTE","FORMATEUR",1,a1,s1);
            //Candidature c2 = new Candidature("EN ATTENTE","STAGIAIRE",1,a2,s2);

            prerequisRepository.saveAll(List.of(p1,p2));
            agentRepository.saveAll(List.of(a1,a2,a3));
            typeSessionUVRepository.saveAll(List.of(ts1,ts2,ts3));
            sessionUVRepository.saveAll(List.of(s1,s2,s3));
            candidatureRepository.saveAll(List.of(c1));

        };

    }
}

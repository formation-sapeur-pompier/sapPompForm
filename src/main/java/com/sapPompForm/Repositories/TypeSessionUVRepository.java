package com.sapPompForm.Repositories;

import com.sapPompForm.entities.TypeSessionUV;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeSessionUVRepository extends JpaRepository<TypeSessionUV, Integer> {
}

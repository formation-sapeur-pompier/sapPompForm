package com.sapPompForm.Repositories;

import com.sapPompForm.entities.Agent;
import com.sapPompForm.entities.Candidature;
import com.sapPompForm.entities.SessionUV;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidatureRepository extends JpaRepository<Candidature, Integer> {
    @Query("SELECT count(*) FROM Candidature AS c WHERE c.sessionUV = ?1 ")
    int findBySessionUV(SessionUV sessionUV);

    @Query("SELECT agent FROM Candidature AS c WHERE c.idCandidature = ?1")
    Agent findByAgent(int idCandidature);
}

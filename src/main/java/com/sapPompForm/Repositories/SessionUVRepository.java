package com.sapPompForm.Repositories;

import com.sapPompForm.entities.SessionUV;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionUVRepository extends JpaRepository<SessionUV, Integer> {
}

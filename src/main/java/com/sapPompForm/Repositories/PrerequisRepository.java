package com.sapPompForm.Repositories;

import com.sapPompForm.entities.Prerequis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PrerequisRepository extends JpaRepository<Prerequis, Integer> {
    Optional<Prerequis> findByTitle(String title);
}

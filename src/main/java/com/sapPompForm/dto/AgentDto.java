package com.sapPompForm.dto;

import com.sapPompForm.entities.Agent;
import com.sapPompForm.entities.Candidature;
import com.sapPompForm.entities.Prerequis;

import java.util.List;

public class AgentDto {
    private int idAgent;
    private String name;
    private String password;
    private List<Candidature> listCandidature;
    private List<Prerequis> certifObtenus;

    public AgentDto() {
    }

    public AgentDto(String name, String password, List<Candidature> listCandidature, List<Prerequis> certifObtenus) {
        this.name = name;
        this.password = password;
        this.listCandidature = listCandidature;
        this.certifObtenus = certifObtenus;
    }


    public int getIdAgent() {
        return idAgent;
    }

    public void setIdAgent(int idAgent) {
        this.idAgent = idAgent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Candidature> getListCandidature() {
        return listCandidature;
    }

    public void setListCandidature(List<Candidature> listCandidature) {
        this.listCandidature = listCandidature;
    }

    public List<Prerequis> getCertifObtenus() {
        return certifObtenus;
    }

    public void setCertifObtenus(List<Prerequis> certifObtenus) {
        this.certifObtenus = certifObtenus;
    }
}

package com.sapPompForm.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sapPompForm.dto.AgentDto;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
public class Agent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idAgent;
    private String name;
    private String password;
    @ElementCollection(targetClass = Role.class)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "AGENT_ROLE",joinColumns = @JoinColumn(name = "AGENT_ID"))
    private Collection<Role> role;
    @OneToMany(mappedBy = "agent")
    private List<Candidature> listCandidature;
    @ManyToMany
    @JoinTable(name = "AGENT_CERTIF", joinColumns = @JoinColumn(name = "AGENT_ID"), inverseJoinColumns = @JoinColumn(name = "PREREQUIS_ID"))
    private List<Prerequis> certifObtenus;

    public Agent() {
    }

    public Agent(String name, String password, Collection<Role> collectionRole, List<Candidature> listCandidature, List<Prerequis> certifObtenus ) {
        this.name = name;
        this.password = password;
        this.role = collectionRole;
        this.listCandidature= listCandidature;
        this.certifObtenus = certifObtenus;
    }


    public int getIdAgent() {
        return idAgent;
    }

    public void setIdAgent(int idAgent) {
        this.idAgent = idAgent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Role> getRole() {
        return role;
    }

    public void setRole(Collection<Role> role) {
        this.role = role;
    }

    public List<Candidature> getListCandidature() {
        return listCandidature;
    }

    public void setListCandidature(List<Candidature> listCandidature) {
        this.listCandidature = listCandidature;
    }

    public List<Prerequis> getCertifObtenus() {
        return certifObtenus;
    }

    public void setCertifObtenus(List<Prerequis> certifObtenus) {
        this.certifObtenus = certifObtenus;
    }
}

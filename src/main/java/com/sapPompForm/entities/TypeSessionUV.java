package com.sapPompForm.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class TypeSessionUV {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTypeSessionUV;
    private String title;
    private int minNumber;
    private int maxNumber;
    @OneToMany(mappedBy = "typeSessionUV")
    @JsonBackReference
    private List<SessionUV> listSessionsUV;
    @OneToMany
    @JoinTable(name = "TYPESESSION_PREREQUIS", joinColumns = @JoinColumn(name = "TYPESESSION_ID"), inverseJoinColumns = @JoinColumn(name = "PREREQUIS_ID"))
    private List<Prerequis> listPrerequis;

    public TypeSessionUV() {
    }

    public TypeSessionUV(String title, int minNumber, int maxNumber, List<Prerequis> listPrerequis) {
        this.title = title;
        this.minNumber = minNumber;
        this.maxNumber = maxNumber;
        this.listPrerequis = listPrerequis;
    }

    public int getIdTypeSessionUV() {
        return idTypeSessionUV;
    }

    public void setIdTypeSessionUV(int idTypeSessionUV) {
        this.idTypeSessionUV = idTypeSessionUV;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMinNumber() {
        return minNumber;
    }

    public void setMinNumber(int minNumber) {
        this.minNumber = minNumber;
    }

    public int getMaxNumber() {
        return maxNumber;
    }

    public void setMaxNumber(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    public List<SessionUV> getListSessionsUV() {
        return listSessionsUV;
    }

    public void setListSessionsUV(List<SessionUV> listSessionsUV) {
        this.listSessionsUV = listSessionsUV;
    }

    public List<Prerequis> getListPrerequis() {
        return listPrerequis;
    }

    public void setListPrerequis(List<Prerequis> listPrerequis) {
        this.listPrerequis = listPrerequis;
    }
}

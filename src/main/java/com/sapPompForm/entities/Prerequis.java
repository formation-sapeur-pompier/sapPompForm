package com.sapPompForm.entities;


import javax.persistence.*;

@Entity
public class Prerequis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPrerequis;
    private String title;

    public Prerequis() {
    }

    public Prerequis(String title) {
        this.title = title;
    }

    public int getIdPrerequis() {
        return idPrerequis;
    }

    public void setIdPrerequis(int idPrerequis) {
        this.idPrerequis = idPrerequis;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}

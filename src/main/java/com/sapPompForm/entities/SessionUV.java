package com.sapPompForm.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class SessionUV {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idSessionUV;
    private String startDate;
    private String endDate;
    private String formateur;
    @OneToMany(mappedBy = "sessionUV")
    @JsonBackReference
    private List<Candidature> listCandidature;
    @ManyToOne
    @JoinColumn(name = "idTypeSessionUV")
    private TypeSessionUV typeSessionUV;

    public SessionUV() {
    }

    public SessionUV(String startDate, String endDate, String formateur, TypeSessionUV typeSessionUV) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.typeSessionUV = typeSessionUV;
        this.formateur = formateur;
    }

    public int getIdSessionUV() {
        return idSessionUV;
    }

    public void setIdSessionUV(int idSessionUV) {
        this.idSessionUV = idSessionUV;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFormateur() {
        return formateur;
    }

    public void setFormateur(String formateur) {
        this.formateur = formateur;
    }

    public List<Candidature> getListCandidature() {
        return listCandidature;
    }

    public void setListCandidature(List<Candidature> listCandidature) {
        this.listCandidature = listCandidature;
    }

    public TypeSessionUV getTypeSessionUV() {
        return typeSessionUV;
    }

    public void setTypeSessionUV(TypeSessionUV typeSessionUV) {
        this.typeSessionUV = typeSessionUV;
    }
}

package com.sapPompForm.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Candidature {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idCandidature;
    private String statut;
    private String qualite;
    private int position;
    @ManyToOne
    @JoinColumn(name="idAgent")
    @JsonBackReference
    private Agent agent;
    @ManyToOne
    @JoinColumn(name="idSessionUV")
    private SessionUV sessionUV;

    public Candidature() {
    }


    public Candidature(int idCandidature, String statut, String qualite, int position, Agent agent, SessionUV sessionUV) {
        this.idCandidature = idCandidature;
        this.statut = statut;
        this.qualite = qualite;
        this.position = position;
        this.agent = agent;
        this.sessionUV = sessionUV;
    }


    public Candidature(String statut, String qualite, int position, Agent agent, SessionUV sessionUV) {
        this.statut = statut;
        this.qualite = qualite;
        this.position = position;
        this.agent = agent;
        this.sessionUV = sessionUV;
    }

    public int getIdCandidature() {
        return idCandidature;
    }

    public void setIdCandidature(int idCandidature) {
        this.idCandidature = idCandidature;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getQualite() {
        return qualite;
    }

    public void setQualite(String qualite) {
        this.qualite = qualite;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public SessionUV getSessionUV() {
        return sessionUV;
    }

    public void setSessionUV(SessionUV sessionUV) {
        this.sessionUV = sessionUV;
    }
}

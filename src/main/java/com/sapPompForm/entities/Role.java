package com.sapPompForm.entities;

public enum Role {
    USER,
    ADMIN
}

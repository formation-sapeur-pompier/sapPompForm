package com.sapPompForm.services;

import com.sapPompForm.Repositories.TypeSessionUVRepository;
import com.sapPompForm.entities.TypeSessionUV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeSessionUVService {
    @Autowired
    private TypeSessionUVRepository typeSessionUVRepository;

    public List<TypeSessionUV> listTypeSession() {
        return typeSessionUVRepository.findAll();
    }

    public void addNewTypeSessionUV(TypeSessionUV typeSessionUV) {
        if (typeSessionUV.getMinNumber()<=0 || typeSessionUV.getMaxNumber()<= typeSessionUV.getMinNumber())
            throw new IllegalStateException("Verify MinNumber if > 0 and MaxNumber > MinNumber");
        typeSessionUVRepository.save(typeSessionUV);
    }

    public void updateTypeSession(TypeSessionUV typeSessionUV) {
        TypeSessionUV existTypeSession = typeSessionUVRepository.findById(typeSessionUV.getIdTypeSessionUV())
                .orElseThrow(() -> new IllegalStateException("This typeSessionUV not exist"));
        if (typeSessionUV.getMinNumber()<=0 || typeSessionUV.getMaxNumber()<= typeSessionUV.getMinNumber())
            throw new IllegalStateException("Verify MinNumber if > 0 and MaxNumber > MinNumber");
        existTypeSession.setTitle(typeSessionUV.getTitle());
        existTypeSession.setMinNumber(typeSessionUV.getMinNumber());
        existTypeSession.setMaxNumber(typeSessionUV.getMaxNumber());
        existTypeSession.setListPrerequis(typeSessionUV.getListPrerequis());
        typeSessionUVRepository.save(existTypeSession);
    }

    public void deleteTypeSessionUV(int idTypeSessionUV) {
        TypeSessionUV existTypeSession = typeSessionUVRepository.findById(idTypeSessionUV)
                .orElseThrow(() -> new IllegalStateException("This typeSessionUV not exist"));
        typeSessionUVRepository.delete(existTypeSession);
    }
}

package com.sapPompForm.services;

import com.sapPompForm.Repositories.AgentRepository;
import com.sapPompForm.Repositories.CandidatureRepository;
import com.sapPompForm.Repositories.PrerequisRepository;
import com.sapPompForm.dto.AgentDto;
import com.sapPompForm.entities.Agent;
import com.sapPompForm.entities.Candidature;
import com.sapPompForm.entities.Prerequis;
import com.sapPompForm.entities.SessionUV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CandidatureService {
    @Autowired
    private CandidatureRepository candidatureRepository;
    @Autowired
    private AgentRepository agentRepository;
    @Autowired
    private AgentService agentService;
    @Autowired
    private SessionUVService sessionUVService;
    @Autowired
    private PrerequisRepository prerequisRepository;


    public List<Candidature> listCandidature() {
        return candidatureRepository.findAll();
    }

    public void addNewCandidature(int idAgent, Candidature candidature) {
        Date currentDate = new Date();
        AgentDto getOneAgent = agentService.getOneAgent(idAgent);
        SessionUV getSession = sessionUVService.oneSessionUV(candidature.getSessionUV().getIdSessionUV());

        try {
            Date dateStart = new SimpleDateFormat("dd/MM/yyyy").parse(getSession.getStartDate());
            if (dateStart.before(currentDate)||dateStart.compareTo(currentDate)==0)
                throw new IllegalStateException("the start of the sessionUV has passed");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (candidature.getQualite().length() == 0) throw new IllegalStateException("Quality is required for candidature");
        boolean candidatureExist = getOneAgent.getListCandidature().stream().anyMatch(c -> c.getQualite().equals(candidature.getQualite()));
        if (candidatureExist) throw new IllegalStateException("You have applicate this candidature");

        List<Prerequis> listPrerequis = getSession.getTypeSessionUV().getListPrerequis();
        if (!listPrerequis.isEmpty()) {
            boolean equalsCertifAndPrereq = getOneAgent.getCertifObtenus().stream()
                    .anyMatch(pre -> listPrerequis.stream().anyMatch(certif->certif.getTitle().equals(pre.getTitle())));
            if (!equalsCertifAndPrereq) throw new IllegalStateException("not have the prerequis "+listPrerequis.stream().map(Prerequis::getTitle).collect(Collectors.toList()));
        }
        Agent oneAgent = agentService.getAgentFromDto(getOneAgent);
        if (candidature.getQualite().equals("FORMATEUR")){
            boolean haveCertif =oneAgent.getCertifObtenus().stream().anyMatch(cer->cer.getTitle().equals(candidature.getSessionUV().getTypeSessionUV().getTitle()));
            if (!haveCertif) throw new IllegalStateException("not having the certif to teach this session");
        }
        candidature.setAgent(oneAgent);
        candidature.setStatut("EN ATTENTE");
        candidatureRepository.save(candidature);
    }

    private int countCandidatureforSession(SessionUV sessionUV){
        int count = candidatureRepository.findBySessionUV(sessionUV);
        return count;
    }
    /*
    Pas possible de modifier une candidature après avoir fini de candidater

    public void updateCandidature(int idAgent, Candidature candidature) {
        AgentDto getOneAgent = agentService.getOneAgent(idAgent);
        Candidature candidatureExist = getOneAgent.getListCandidature().stream()
                .filter(cand -> cand.getIdCandidature() == candidature.getIdCandidature()).findFirst()
                .orElseThrow(() -> new IllegalStateException("This candidature is not exist"));
        candidatureExist.setQualite(candidature.getQualite());
        candidatureExist.setStatut(candidature.getStatut());
        candidatureExist.setSessionUV(candidature.getSessionUV());
        candidatureExist.setPosition(candidature.getPosition());
        candidatureRepository.save(candidatureExist);
    }
*/

    public void deleteCandidature(int idAgent, int idCandidature) {
        AgentDto getOneAgent = agentService.getOneAgent(idAgent);
        Candidature candidatureExist = getOneAgent.getListCandidature().stream()
                .filter(cand -> cand.getIdCandidature() == idCandidature).findFirst()
                .orElseThrow(() -> new IllegalStateException("candidature id "+ idCandidature +" is not exist"));
        candidatureRepository.delete(candidatureExist);
    }

    public Candidature oneCandidature(int idCandidature) {
        Candidature candidature = candidatureRepository.findById(idCandidature)
                .orElseThrow(() -> new IllegalStateException("candidature id "+ idCandidature +" is not exist"));
        return candidature;
    }

    public void updateStatus(int idCandidature, String status) {
        Candidature candidature = candidatureRepository.findById(idCandidature)
                .orElseThrow(()-> new IllegalStateException("candidature id "+ idCandidature +" is not exist"));

        if (candidature.getQualite().equals("STAGIAIRE")) {
            int nberCandidatureSession = countCandidatureforSession(candidature.getSessionUV());
            int maxNberCandSession = candidature.getSessionUV().getTypeSessionUV().getMaxNumber();
            if (nberCandidatureSession >= maxNberCandSession) throw new IllegalStateException("full for this sessionUV");
        } else if (candidature.getQualite().equals("FORMATEUR") && candidature.getSessionUV().getFormateur()!=null) {
            throw new IllegalStateException("this session have the formateur");
        }

        candidature.setStatut(status.toUpperCase());
        String title =  candidature.getSessionUV().getTypeSessionUV().getTitle();
        Agent agentByCand = candidatureRepository.findByAgent(idCandidature);

        if (candidature.getQualite().equals("FORMATEUR")){
            candidature.getSessionUV().setFormateur(agentByCand.getName());
        }
        candidatureRepository.save(candidature);
        Optional<Prerequis> prerequis = prerequisRepository.findByTitle(title);
        ArrayList<Prerequis> certif = new ArrayList<>();
        prerequis.ifPresent(certif::add);
        agentByCand.setCertifObtenus(certif);
        agentRepository.save(agentByCand);
    }
}

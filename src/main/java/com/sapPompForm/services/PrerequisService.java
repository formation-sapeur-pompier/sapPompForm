package com.sapPompForm.services;

import com.sapPompForm.Repositories.PrerequisRepository;
import com.sapPompForm.entities.Prerequis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PrerequisService {
    @Autowired
    private PrerequisRepository prerequisRepository;

    public List<Prerequis> listPrerequis() {
        return prerequisRepository.findAll();
    }

    public void addNewPrerequis(Prerequis prerequis) {
        Optional<Prerequis> prerequisByTitle = prerequisRepository.findByTitle(prerequis.getTitle());
        if (prerequisByTitle.isPresent()) throw new IllegalStateException("The prerequis " +prerequis.getTitle()+" exist");
        if (prerequis.getTitle().length() == 0 ) throw new IllegalStateException("Title required");
        prerequisRepository.save(prerequis);
    }

    public void updatePrerequis(Prerequis prerequis) {
        Prerequis prerequisById = prerequisRepository.findById(prerequis.getIdPrerequis())
                .orElseThrow(()-> new IllegalStateException("The prerequis " +prerequis.getIdPrerequis()+" not exist"));
        if (prerequis.getTitle().length() == 0 ) throw new IllegalStateException("Title required");
        prerequisById.setTitle(prerequis.getTitle());
        prerequisRepository.save(prerequisById);
    }

    public void deletePrerequis(int idPrerequis) {
        Prerequis prerequisById = prerequisRepository.findById(idPrerequis)
                .orElseThrow(()-> new IllegalStateException("The prerequis " +idPrerequis+" not exist"));
        prerequisRepository.delete(prerequisById);
    }

    public Prerequis onePrerequis(int idPrerequis) {
        Prerequis prerequisById = prerequisRepository.findById(idPrerequis)
                .orElseThrow(()-> new IllegalStateException("The prerequis " +idPrerequis+" is not exist"));
        return prerequisById;
    }
}

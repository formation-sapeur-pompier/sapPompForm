package com.sapPompForm.services;

import com.sapPompForm.Repositories.AgentRepository;
import com.sapPompForm.dto.AgentDto;
import com.sapPompForm.entities.Agent;
import com.sapPompForm.entities.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class AgentService {
    @Autowired
    private AgentRepository agentRepository;

    public List<Agent> allAgent(){
        return agentRepository.findAll();
    }

    public void addNewAgent(AgentDto agentDto) {
        Optional<Agent> agentByName = agentRepository.findAgentByName(agentDto.getName());
        if (agentByName.isPresent()) throw new IllegalStateException("Name taken");
        Agent newAgent = getAgentFromDto(agentDto);

        newAgent.setRole(List.of(Role.USER));
        if (newAgent.getName().equals("ADMIN")){
            newAgent.setRole(List.of(Role.ADMIN, Role.USER));
        }
        agentRepository.save(newAgent);
    }

    public void deleteAgent(int idAgent) {
        boolean exists = agentRepository.existsById(idAgent);
        if (!exists) throw new IllegalStateException("Agent with id "+idAgent+" does not exists");
        agentRepository.deleteById(idAgent);
    }

    public void updateAgent(AgentDto agentDto) {

        Agent agentById = agentRepository.findById(agentDto.getIdAgent())
                .orElseThrow(() -> new IllegalStateException("Agent with id "+agentDto.getIdAgent()+" does not exists"));
        Optional<Agent> agentByName = agentRepository.findAgentByName(agentDto.getName());
        if (agentByName.isPresent()) throw new IllegalStateException("Name taken");
        if (agentDto.getName() != null &&
                agentDto.getName().length()>0 &&
                !Objects.equals(agentDto.getName(), agentById.getName())){

            agentById.setName(agentDto.getName());
        }
        if (agentDto.getPassword() != null &&
                agentDto.getPassword().length() > 0 &&
                !Objects.equals(agentDto.getPassword(), agentById.getPassword())){

            agentById.setPassword(agentDto.getPassword());
        }
        agentRepository.save(agentById);
    }

    public AgentDto getOneAgent(int idAgent) {
        Agent agentById = agentRepository.findById(idAgent)
                .orElseThrow(() -> new IllegalStateException("Agent with id "+idAgent+" does not exists"));
        AgentDto oneAgentDto = getDtoFromAgent(agentById);

        return oneAgentDto;
    }

    public AgentDto getDtoFromAgent(Agent agent) {
        AgentDto agentDto = new AgentDto();
        agentDto.setIdAgent(agent.getIdAgent());
        agentDto.setName(agent.getName());
        agentDto.setPassword(agent.getPassword());
        agentDto.setListCandidature(agent.getListCandidature());
        agentDto.setCertifObtenus(agent.getCertifObtenus());
        return agentDto;
    }

    public Agent getAgentFromDto(AgentDto agentDto){
        Agent agent = new Agent();
        agent.setIdAgent(agentDto.getIdAgent());
        agent.setName(agentDto.getName());
        agent.setPassword(agentDto.getPassword());
        agent.setListCandidature(agentDto.getListCandidature());
        agent.setCertifObtenus(agentDto.getCertifObtenus());
        return agent;
    }
}

package com.sapPompForm.services;

import com.sapPompForm.Repositories.SessionUVRepository;
import com.sapPompForm.entities.SessionUV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SessionUVService {
    @Autowired
    private SessionUVRepository sessionUVRepository;

    public List<SessionUV> listSession() {
        return sessionUVRepository.findAll();
    }

    public void addNewSessionUV(SessionUV sessionUV) {
        if (sessionUV.getStartDate().isEmpty() && sessionUV.getEndDate().isEmpty())
            throw new IllegalStateException("Required StartDate, EndDate");

        try {
            Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse(sessionUV.getStartDate());
            Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse(sessionUV.getEndDate());
            if (startDate != null && startDate.compareTo(endDate) > 0) throw new IllegalStateException("Date start is After Date end");

        } catch (ParseException e) {
            e.printStackTrace();
        }
        sessionUVRepository.save(sessionUV);
    }

    public void updateSessionUV(SessionUV sessionUV) {
        SessionUV sessionUVById = sessionUVRepository.findById(sessionUV.getIdSessionUV())
                .orElseThrow(()-> new IllegalStateException("This sessionUV is not exist"));

        if (sessionUV.getStartDate().isEmpty() && sessionUV.getEndDate().isEmpty())
            throw new IllegalStateException("Required StartDate, EndDate");

        try {
            Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse(sessionUV.getStartDate());
            Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse(sessionUV.getEndDate());

            if (startDate != null && startDate.compareTo(endDate) > 0) throw new IllegalStateException("Date start is After Date end");

        } catch (ParseException e) {
            e.printStackTrace();
        }

        sessionUVById.setStartDate(sessionUV.getStartDate());
        sessionUVById.setEndDate(sessionUV.getEndDate());
        sessionUVById.setTypeSessionUV(sessionUV.getTypeSessionUV());
        sessionUVRepository.save(sessionUVById);
    }

    public void deleteSessionUV(int idSessionUV) {
        SessionUV sessionUVById = sessionUVRepository.findById(idSessionUV)
                .orElseThrow(()-> new IllegalStateException("This sessionUV is not exist"));
        sessionUVRepository.delete(sessionUVById);
    }

    public SessionUV oneSessionUV(int idSessionUV) {
        SessionUV sessionUVById = sessionUVRepository.findById(idSessionUV)
                .orElseThrow(()-> new IllegalStateException("This sessionUV is not exist"));
        return sessionUVById;
    }

    public List<SessionUV> listCurrentSessionUV() {
        Date currentDate = new Date();
        List <SessionUV> listSessionsUV = sessionUVRepository.findAll();
        List<SessionUV> listCurrentSessionUV = new ArrayList<>();
        for (SessionUV p : listSessionsUV) {
            try {
                Date dateStart = new SimpleDateFormat("dd/MM/yyyy").parse(p.getStartDate());
                Date dateEnd =  new SimpleDateFormat("dd/MM/yyyy").parse(p.getEndDate());
                if (dateStart.compareTo(currentDate)<0 && dateEnd.compareTo(currentDate)>0 ) {
                    listCurrentSessionUV.add(p);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return listCurrentSessionUV;
    }

    public List<SessionUV> listNextSessionUV() {
        Date currentDate = new Date();
        List <SessionUV> listSessionsUV = sessionUVRepository.findAll();
        List<SessionUV> listNextSessionUV = new ArrayList<>();
        for (SessionUV p : listSessionsUV) {
            try {
                Date dateStart = new SimpleDateFormat("dd/MM/yyyy").parse(p.getStartDate());
                if (dateStart.after(currentDate)) {
                    listNextSessionUV.add(p);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return listNextSessionUV;
    }
}

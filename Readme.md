## **Gérer des stages sapeurs-pompiers**

### Agent
`Un agent est une personne titulaire d’aptitudes via les unités de valeur.`

### Unité de valeur (UV)
`C’est une aptitude donnée obtenue suite à une session de formation (Stage)
`
### Stage
`C’est l’organisation d’une session de formation. Il permet d’obtenir une ou plusieurs UV
`
### Formateur
`Un agent formateur  participe comme formateur à une session de formation (stage).Ceci 
demande également des compétences (aptitudes , UV detenues).`

### Stagiaire
`Un agent qui participe à un stage en temps qu’apprenant.`

`Un stage comporte un nombre minimum et maximum de stagiaires.`

`La participation n’est possible qu’à condition d’avoir des UV acquises (prérequis).
`
### Candidat
`Un agent qui depose une candidature pour une session de formation.`
`Le candidat peut être stagiaire ou un formateur.`
